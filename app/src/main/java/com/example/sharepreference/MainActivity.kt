package com.example.sharepreference

import android.os.Bundle
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {

    private lateinit var name: EditText
    private lateinit var age: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        name = findViewById(R.id.edit1)
        age = findViewById(R.id.edit2)
    }

    override fun onPause() {
        super.onPause()
        val sharedPref = this@MainActivity.getSharedPreferences("MySharedPref", MODE_PRIVATE)
        with(sharedPref.edit()){
            putString("name",name.text.toString())
            putInt("age",age.text.toString().toInt())
            apply()
        }
    }

    override fun onResume() {
        super.onResume()
        val sh = getSharedPreferences("MySharedPref", MODE_PRIVATE)
        name.setText(sh.getString("name",""))
        age.setText(sh.getInt("age",0).toString())
    }
}